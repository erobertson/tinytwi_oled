#define START 0xA4
#define SEND 0x84
#define GET 0xC4
#define STOP 0x94
#define TWEN 0x04
//#define TWINT 0x80
#define SLA 0x13
#define READ 0x01
#define WRITE 0x00
#define TXC 0x40
//#define NULL 0x00
/*
#define TWBR 0x10
#define TWSR 0x0E
#define TWCR 0x0D
#define TWINT   0x80
*/
#define USI_START_INT USISIF


#if defined (__AVR_ATtiny25__) || defined (__AVR_ATtiny45__) || defined (__AVR_ATtiny85__)
#define TWDR USIDR
#define TWBR USICR
#define TWSR USISR
#define TWCR USICR
#define TWEA 6 //probably wrong?
#define TWINT USI_START_INT
#define USEUSI
#endif
