/*
 * twi.c
 *
 *  Created on: May 15, 2016
 *      Author: eric
 */
#include <avr/io.h>
#include "twi.h"
#include "TWIDefines.h"

void twiInit(void){
	TWBR = TWIBITRATE;

	#if defined(USEUSI)
		TWCR = (1 << USISIE) | //start condition interrupt
		(0 << USIOIE) | //overflow interrupt
		(1 << USIWM1) | //two-wire mode
		(0 << USIWM0) | //set overflow behavior to not overflow with require reset
		(0 << USICS1) | //counter source 1
		(0 << USICS0) | //counter source 0 (
		(1 << USICLK) | //clock source. 0 is external, 1 is USICLK
		(0 << USITC); //toggle clock
	#else
		TWCR |= (1 << TWEN);

	#endif
	//USIIR needs to be set as well
}

void twiWaitComplete(){ //twint is set when an operation is complete
	while (!(TWCR & (1<<TWINT)))
		;
}

void twiStart(uint8_t addrMode){

	TWCR = START;
	TWDR = addrMode;
	twiWaitComplete();
	//send slave address

	//send read/write
}

/* need to add error checking after each stage
uint8_t checkError(uint8_t cmphex){
	uint8_t error = 0;
	if(TWSR & cmphex)
}*/

void twiStop(void){
	TWCR = STOP;
	twiWaitComplete();
}

void twiSend(uint8_t data, uint8_t addrMode){
	twiStart(addrMode);
	TWDR = data;
	TWCR = (_BV(TWINT) | _BV(TWEN));
	twiWaitComplete();
	twiStop();
}

uint8_t twiReadAck(void){
	//sends TWEA
	TWCR = (_BV(TWINT) | _BV(TWEN) | _BV(TWEA));
	twiWaitComplete();
	return(TWDR);
}

uint8_t twiReadNACK(){
	//no TWEA
	//TWDR = data;
	//need to double-check above
	TWCR = _BV(TWINT) | _BV(TWEN);
	twiWaitComplete();

}

