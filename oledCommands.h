/*
 * oledCommands.h
 *
 *  Created on: May 16, 2016
 *      Author: eric
 */

//Command values and init sequencing borrowed from:
//https://github.com/tibounise/SSD1306-AVR

  // create command buffers
#include <inttypes.h>
#define SSD1306_DEFAULT_ADDRESS 0x78
#define SSD1306_SETCONTRAST 0x81
#define SSD1306_DISPLAYALLON_RESUME 0xA4
#define SSD1306_DISPLAYALLON 0xA5
#define SSD1306_NORMALDISPLAY 0xA6
#define SSD1306_INVERTDISPLAY 0xA7
#define SSD1306_DISPLAYOFF 0xAE
#define SSD1306_DISPLAYON 0xAF
#define SSD1306_SETDISPLAYOFFSET 0xD3
#define SSD1306_SETCOMPINS 0xDA
#define SSD1306_SETVCOMDETECT 0xDB
#define SSD1306_SETDISPLAYCLOCKDIV 0xD5
#define SSD1306_SETPRECHARGE 0xD9
#define SSD1306_SETMULTIPLEX 0xA8
#define SSD1306_SETLOWCOLUMN 0x00
#define SSD1306_SETHIGHCOLUMN 0x10
#define SSD1306_SETSTARTLINE 0x40
#define SSD1306_MEMORYMODE 0x20
#define SSD1306_COLUMNADDR 0x21
#define SSD1306_PAGEADDR   0x22
#define SSD1306_COMSCANINC 0xC0
#define SSD1306_COMSCANDEC 0xC8
#define SSD1306_SEGREMAP 0xA0
#define SSD1306_CHARGEPUMP 0x8D
#define SSD1306_SWITCHCAPVCC 0x2
#define SSD1306_NOP 0xE3
#define SSD1306_MULTIPLEX 0x3F //confirm in datasheet
#define SSD1306_COMPINS 0x12 //confirm in datasheet

uint8_t initSequence[] = {
		SSD1306_DISPLAYOFF,
		SSD1306_SETDISPLAYCLOCKDIV, 0x80,
		SSD1306_SETMULTIPLEX, SSD1306_MULTIPLEX,
		SSD1306_SETDISPLAYOFFSET, 0x00,
		SSD1306_SETSTARTLINE, 0x00,
		SSD1306_CHARGEPUMP, 0x14,
		SSD1306_MEMORYMODE, 0x00,
		SSD1306_SEGREMAP, 0x01,
		SSD1306_COMSCANDEC,
		SSD1306_SETCOMPINS, SSD1306_COMPINS,
		SSD1306_SETCONTRAST, 0xCF,
		SSD1306_SETPRECHARGE, 0xF1,
		SSD1306_SETVCOMDETECT, 0x40,
		SSD1306_DISPLAYALLON_RESUME,
		SSD1306_NORMALDISPLAY,
		SSD1306_DISPLAYON
};


