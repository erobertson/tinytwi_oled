#include <inttypes.h>

#define TWIBITRATE 32;

void twiInit(void); //init to 100khz

void twiWaitComplete(void); //wait for TWINT

void twiStart(uint8_t addrMode); //send TWSTA (start)

void twiStop(void); //send TWOSTO (stop)

void twiSend(uint8_t data, uint8_t addrMode);

uint8_t twiReadAck(void); //sends TWEA

uint8_t twiReadNACK(void); //no TWEA
