#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <util/twi.h>
#include <avr/io.h>
#include <util/delay.h>

#include "twi.h"
#include "TWIDefines.h"
#include "oledCommands.h"

//power reduction register: PRTWI needs to be set 0 to prevent TWI from being disabled

//poll TWINT:
//while(!(TWCR&(1<<TWINT)));
//check status register
//TWSR 0x08 or TW_START

//start condition:
//write 1 to TWINT, TWSTA and TWEN

#define OLED_ADDR 0x78
#define OLED_WRITE 0x78 << 1
#define OLED_READ 0x78 << 1 | 1
#define SCL_PIN PB0
#define SDA_PIN PB2
#define LED0 PB3
#define LED1 PB4
#define LED2 PB1

/*
void send(uint8_t data){
	TWCR = data;
	TWCR = (_BV(TWINT) | _BV(TWEN));
	twiWaitComplete();
}

void stop(void){
	TWCR = STOP;
}*/

uint8_t fillBuf[] =
{
		0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0XFF,
		0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0XFF,
		0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0XFF,
		0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0XFF
};

void sendFramebuffer(uint8_t *buffer) {
    twiSend(SSD1306_COLUMNADDR, OLED_WRITE);
    twiSend(0x00, OLED_WRITE);
    twiSend(0x7F, OLED_WRITE);

    twiSend(SSD1306_PAGEADDR, OLED_WRITE);
    twiSend(0x00, OLED_WRITE);
    twiSend(0x07, OLED_WRITE);

    // We have to send the buffer as 16 bytes packets
    // Our buffer is 1024 bytes long, 1024/16 = 64
    // We have to send 64 packets
    for (uint8_t packet = 0; packet < 64; packet++) {
        twiStart(OLED_WRITE);
        twiSend(0x40, OLED_WRITE);
        for (uint8_t packet_byte = 0; packet_byte < 16; ++packet_byte) {
            twiSend(buffer[packet*16+packet_byte], OLED_WRITE);
        }
        twiStop();
    }
}




int main(){

	//set data direction to output
	DDRB |= (1 << SCL_PIN) | (1 << SDA_PIN) | (1 << LED0) | (1 << LED1) | (1 << LED2);
	twiInit();
	sendFramebuffer(fillBuf);
	while(1){
		//turn off LEDs

		PORTB &= ~(1 << LED0);
		_delay_ms(500);
		PORTB &= ~(1 << LED1);
		PORTB &= ~(1 << LED2);
		_delay_ms(500);
		//turn on LEDs
		PORTB |= (1 << LED0);
		_delay_ms(500);
		PORTB |= (1 << LED1);
		PORTB |= (1 << LED2);
		_delay_ms(500);

	}

	return 0;
}
